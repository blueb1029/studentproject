/* Bryan Blue
    11/16/18
    TCES 203
    Assignment #4

This file contains all of the constructors/functions for the Student object.
*/
#include "Student.h"
using namespace std;

//Default constructor
Student::Student()
{
    name = "";
    hobby = "";
    SID = 0;
}

//Overloaded constructor
Student::Student(string name, string hobby, int SID)
{
    this->name = name;
    this->hobby = hobby;
    this->SID = SID;
}

//Copy constructor
Student::Student(const Student &studentCopy)
{
    name = studentCopy.name;
    hobby = studentCopy.hobby;
    SID = studentCopy.SID;
}

//Prints out student's full name, hobby, and ID
void Student::print()
{
    cout << "Name: " << name << endl;
    cout << "Hobby: " << hobby << endl;
    cout << "Student ID: " << SID << endl;
}

//Sets name for student
void Student::setName(const string name)
{
    this->name = name;
}

//Sets hobby for student
void Student::setHobby(const string hobby)
{
    this->hobby = hobby;
}

//Deconstructor
Student::~Student()
{

}


