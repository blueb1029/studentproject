/* Bryan Blue
    11/16/18
    TCES 203
    Assignment #4

The purpose of this program is to have the user input student information
and be able modify/view it. The type of information that is stored is the
student's full name, hobby, and a 5 number ID. After the user specifies how
many students there are and inputs all their information, they can then get
any information on a student, or set their name or hobby. This program only
assumes the user will input the correct data types (integers or strings)
when appropriate.
*/
#include <iostream>
#include <Student.h>
#include <string>

using namespace std;

//These are the options for the user interface
typedef enum
{
    QUIT,
    PRINT,
    GETNAME,
    GETHOBBY,
    GETSID,
    SETNAME,
    SETHOBBY,
} options;

void initalizeStudent(Student &);
void studentInterface(Student *, int);
int getOption();
int getStudentIndexSID(Student *, int);
int getStudentIndexName(Student *, int);

int main()
{
    int numStudents, i;
    cout << "Input number of students: ";
    cin >> numStudents;
    cout << endl;
    Student *students = new Student[numStudents];
    for(i = 0; i < numStudents; i++) {
        cout << "Student " << i + 1 << endl;
        initalizeStudent(students[i]);
        cout << endl;
    }
    studentInterface(students, numStudents);
    delete [] students;
    return 0;
}

//Initializes a single student, asks user for the students
//full name, hobby, and 5 number ID
void initalizeStudent(Student &student) {
    string firstName, lastName, hobby;
    int SID;
    cout << "Input student's first name: ";
    cin >> firstName;
    cout << "Input student's last name: ";
    cin >> lastName;
    cout << "Input student's hobby: ";
    cin >> hobby;
    cout << "Input student ID: ";
    cin >> SID;
    while(!((SID >= 10000) && (SID <= 99999))) {
        cout << "Invalid student ID, please input valid ID (5 numbers long): ";
        cin >> SID;
    }
    student = Student(firstName + " " + lastName, hobby, SID);
}

//This function is an user interface that allows the user to interact with the
//student information, the user can input a number from 0 - 8 to either:
//print out a student's information using their ID
//get a student name or hobby using their ID
//get a student ID using their full name
//set a student's name or hobby using their ID
void studentInterface(Student *students, int numStudents) {
    int option = -1, index;
    while(option != QUIT) {
        option = getOption();
        cout << endl;
        switch(option)
            {
                case PRINT:
                    index = getStudentIndexSID(students, numStudents);
                    if(index != -1) {
                        students[index].print();
                    }
                    break;
                case GETNAME:
                    index = getStudentIndexSID(students, numStudents);
                    if(index != -1) {
                        cout << students[index].getName();
                    }
                    break;
                case GETHOBBY:
                    index = getStudentIndexSID(students, numStudents);
                    if(index != -1) {
                        cout << students[index].getHobby();
                    }
                    break;
                case GETSID:
                    index = getStudentIndexName(students, numStudents);
                    if(index != -1) {
                        cout << students[index].getSID();
                    }
                    break;
                case SETNAME:
                    index = getStudentIndexSID(students, numStudents);
                    if(index != -1) {
                        string firstName, lastName;
                        cout << "Please input first name: ";
                        cin >> firstName;
                        cout << "Please input last name: ";
                        cin >> lastName;
                        students[index].setName(firstName + " " + lastName);
                    }
                    break;
                case SETHOBBY:
                    index = getStudentIndexSID(students, numStudents);
                    if(index != -1) {
                        string name;
                        cout << "Please input hobby: ";
                        cin >> name;
                        students[index].setHobby(name);
                    }
                    break;
                case QUIT:
                    cout << "Quitting...\n";
            }
    }
}

//Prints out the user's choices and returns what option they chose
int getOption() {
    int option;
    cout << endl << "---------------------------------" << endl;
    cout << "Please enter your choice: " << endl;
    cout << "\tPrint student information = 1" << endl;
    cout << "\tGet student name = 2" << endl;
    cout << "\tGet student hobby = 3" << endl;
    cout << "\tGet student ID = 4" << endl;
    cout << "\tSet student name = 5" << endl;
    cout << "\tSet student hobby = 6" << endl;
    cout << "\tQuit = 0" << endl;
    cin >> option;
    return option;
}

//Asks the user for a student ID and returns what index that student is
//located in, if it can't find the student, function returns -1
int getStudentIndexSID(Student *students, int numStudents) {
    int id, i;
    cout << "Please input student ID: ";
    cin >> id;
    for(i = 0; i < numStudents; i++) {
        if(id == students[i].getSID()) {
            return i;
        }
    }
    cout << "Couldn't find student ID" << endl;
    return -1;
}

//Asks the user for a student's full name and returns what index that student is
//located in, if it can't find the student, function returns -1
int getStudentIndexName(Student *students, int numStudents) {
    string firstName, lastName, name;
    int i;
    cout << "Please input student first name: ";
    cin >> firstName;
    cout << "Please input student last name: ";
    cin >> lastName;
    name = firstName + " " + lastName;
    for(i = 0; i < numStudents; i++) {
        if(name == students[i].getName()) {
            return i;
        }
    }
    cout << "Couldn't find student name" << endl;
    return -1;
}
