/* Bryan Blue
    11/16/18
    TCES 203
    Assignment #4

The Student object hold three variables, a full name, hobby, and
5 digit ID. It contains functions to print all the information, get
the name, hobby, and ID, and set the name or hobby.
*/
#ifndef STUDENT_H
#define STUDENT_H
#include <string>
#include <iostream>
using namespace std;

class Student
{
    private:
        string name;
        string hobby;
        int SID;
    public:
        //Default constructor
        Student();
        //Overloaded constructor
        Student(string, string, int);
        //Copy constructor
        Student(const Student&);
        //displays information on student
        void print();
        //Setter functions
        void setName(const string);
        void setHobby(const string);
        //Getter functions
        string getName() const { return name; }
        string getHobby() const { return hobby; }
        int getSID() const { return SID; }
        //Deconstructor
        ~Student();
};

#endif // STUDENT_H
